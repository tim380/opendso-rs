use std::thread::sleep;
use std::time::{Duration, Instant};

pub enum InitRetryMode {
    Time = 0,
    Attempts,
}

pub struct InitRetry {
    mode: InitRetryMode,
    value: f32,
    sleep_time: Duration,
    start_time: Instant,
    pub success: bool,
}

impl InitRetry {
    pub fn new(mode: InitRetryMode, value: f32, sleep_sec: f32) -> Self {
        InitRetry {
            mode,
            value,
            sleep_time: Duration::from_micros((sleep_sec * 1_000_000.0) as u64),
            start_time: Instant::now(),
            success: false,
        }
    }

    pub fn attempt_start(&mut self) {
        self.success = false;
        match self.mode {
            InitRetryMode::Time => self.start_time = Instant::now(),
            _ => {}
        }
    }

    pub fn attempt_end(&mut self, success: bool) -> bool {
        if success {
            self.success = success;
            return false;
        }

        let mut retry = false;
        match self.mode {
            InitRetryMode::Time => {
                let useconds = Duration::from_micros(
                    self.start_time.elapsed().as_micros().try_into().unwrap(),
                );
                self.value -= useconds.as_secs_f32();
                if self.value > 0.0 {
                    retry = true;
                }
            }
            InitRetryMode::Attempts => {
                if (self.value - 1.0) > 0.0 {
                    retry = true;
                }
            }
        }

        if self.sleep_time.as_micros() > 0 {
            sleep(self.sleep_time);
        }

        self.success = false;
        retry
    }
}
