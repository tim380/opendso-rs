#![feature(async_closure)]
use futures::future::BoxFuture;
use nats_interface_rs::oes as nats;
use serde::Serialize;
use std::cell::RefCell;
use std::io::Error;
use util::cloud_event::oes as cloud_event;
pub mod heart;

type CloudEvent<T> = cloud_event::CloudEvent<T>;
type NatsWrapper<T> = cloud_event::CloudEventsNatsWrapper<T>;
type HeartStatus = heart::HeartStatus;
type HeartBeat = heart::HeartBeat;

pub struct DsoNatsHeart<T: Sized + Serialize> {
    heart_beat: HeartBeat,
    region_id: Option<String>,
    device_id: Option<String>,
    pub cenats: Option<NatsWrapper<T>>,
}

impl<T> DsoNatsHeart<T>
where
    T: Sized + Serialize,
{
    pub fn generate_nats_topic(&self, extension: Option<String>) -> String {
        fn extend_topic(mut topic: String, extension: Option<String>) -> String {
            match extension {
                Some(extension) => {
                    topic.push_str(&extension[..]);
                    topic
                }
                None => topic,
            }
        }

        let nats_topic = if self.region_id.is_none() || self.device_id.is_none() {
            let mut topic = String::from("opendso.status.");
            topic.push_str(&self.heart_beat.name[..]);
            extend_topic(topic, extension)
        } else {
            let mut topic = String::from("opendso.");
            let region_id = self.region_id.as_ref().unwrap();
            let device_id = self.device_id.as_ref().unwrap();

            topic.push_str(&region_id[..]);
            topic.push_str(".status.");
            topic.push_str(&device_id[..]);
            topic.push('.');
            topic.push_str(&self.heart_beat.name[..]);

            extend_topic(topic, extension)
        };

        nats_topic
    }

    pub async fn wait_for(
        &mut self,
        nats: RefCell<nats::NatsClient<HeartBeat>>,
        subj: String,
        status: HeartStatus,
    ) -> Result<HeartBeat, Error> {
        let mut cenats = NatsWrapper { nats };

        let heart_beat = self.heart_beat.clone();

        let msg = cenats.request_ce(subj.clone(), heart_beat).await.unwrap();
        let msg = std::str::from_utf8(&msg.data).unwrap();

        let event = cloud_event::set_dto::<CloudEvent<HeartBeat>>(msg).unwrap();
        if event.data.status == status {
            return Ok(event.data);
        }

        fn handle(sub: &'_ nats::Subscription) -> BoxFuture<'_, HeartBeat> {
            Box::pin(async {
                loop {
                    match sub.next().await {
                        Some(msg) => {
                            let msg = std::str::from_utf8(&msg.data).unwrap();
                            let event = cloud_event::set_dto::<CloudEvent<HeartBeat>>(msg).unwrap();
                            // a compromise?
                            if event.data.status == HeartStatus::Ready {
                                return event.data;
                            }
                        }
                        None => {
                            panic!("The connection has either been closed or unsubscribed!")
                        }
                    }
                }
            })
        }

        cenats.add_subscription(subj.clone(), handle).await.unwrap();
        Ok(cenats.handle_subscription(subj).await?)
    }
}
