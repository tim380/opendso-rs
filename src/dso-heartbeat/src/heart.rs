use serde::{Deserialize, Serialize};

#[derive(Copy, Clone, Serialize, Deserialize, Debug, Eq, PartialEq)]
pub enum HeartStatus {
    Starting,
    Ready,
    Error,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct HeartBeat {
    pub name: String,
    pub status: HeartStatus,
    pub state: String,
    pub version: String,
}
