#![allow(non_snake_case)]
#![allow(dead_code)]
pub mod opendso {
    extern crate yaml_rust;
    use yaml_rust::{Yaml, YamlLoader};

    use std::collections::HashMap;
    use std::fs::read_to_string;

    #[derive(Debug)]
    pub enum ConfigErr {
        Fail,
        BadFile,
    }

    pub struct Configuration {
        config_info: HashMap<String, Vec<String>>,
    }

    impl Configuration {
        // Parse contents of YAML file into map containing key value pairs
        // of: (regional ID, vector of device MRIDs)
        pub fn load_file(&mut self, _file: &str) -> Result<(), ConfigErr> {
            match read_to_string(_file) {
                Ok(input) => {
                    let config = YamlLoader::load_from_str(&input[..]);
                    if !config.is_ok() {
                        return Err(ConfigErr::BadFile);
                    }
                    let config = config.unwrap();
                    for node in config.iter() {
                        let mut pair: (String, Vec<String>) = (String::new(), Vec::new());
                        match node {
                            Yaml::Hash(map) => {
                                let keys = map.keys();
                                for key in keys {
                                    let values: &Vec<Yaml> =
                                        map.get(&key).unwrap().as_vec().unwrap();
                                    for value in values {
                                        let mrid = value.as_str().unwrap();
                                        pair.1.push(String::from(mrid));
                                    }

                                    let regionId = key.as_str().unwrap();
                                    pair.0 = String::from(regionId);
                                }
                            }
                            _ => {}
                        }
                        self.config_info.insert(pair.0, pair.1);
                    }
                }
                Err(_) => return Err(ConfigErr::Fail),
            };
            Ok(())
        }

        // return the regional ID for a feeder
        // given a device's MRID.
        pub fn get_region(&self, _mrid: String) -> String {
            let map = &self.config_info;
            for (first, second) in map.into_iter() {
                let mrid = _mrid.clone();
                let result = second.iter().any(move |item| item.eq(&mrid[..]));
                if result {
                    return first.to_string();
                }
            }
            String::from("")
        }
    }

    #[cfg(test)]
    #[test]
    fn get_region_test() {
        let mut cfg = Configuration {
            config_info: HashMap::new(),
        };
        cfg.load_file("./config/test.yml").unwrap();

        let test_mrid = String::from("e95669aa-d746-40b8-8c19-7c6251f4403b");
        assert_eq!(
            &cfg.getRegion(test_mrid)[..],
            "9f66d61b-6cd9-4dd9-9a0a-b076f558b4a2"
        );
    }
}
