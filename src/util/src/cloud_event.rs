use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, Debug)]
pub struct CloudEvent<T> {
    pub id: String,
    pub source: Option<String>,
    pub spec_version: String,
    pub _type: Option<String>,
    pub data_content_type: Option<String>,
    pub subject: Option<String>,
    pub time_stamp: Option<String>,
    pub data: T,
}

impl<T> CloudEvent<T> {
    pub fn new(data: T) -> Self
    where
        T: Sized + Serialize,
    {
        CloudEvent {
            id: Uuid::new_v4().to_string(),
            source: None,
            spec_version: String::from("1.0"),
            _type: None,
            data_content_type: Some(String::from("JSON")),
            subject: None,
            time_stamp: None,
            data,
        }
    }
}

pub mod oes {
    pub use super::CloudEvent;
    use futures::future::BoxFuture;
    use nats_interface_rs::oes::{Message, NatsClient, Subscription};
    use serde::{de::Deserialize, Serialize};
    use std::cell::RefCell;
    use std::io::Error;
    use std::result::Result;

    pub struct CloudEventsNatsWrapper<T: Sized + Serialize> {
        pub nats: RefCell<NatsClient<T>>,
    }

    impl<T> CloudEventsNatsWrapper<T>
    where
        T: Sized + Serialize,
    {
        pub async fn publish_ce(&self, subj: String, msg: T) -> Result<(), Error> {
            let response: CloudEvent<T> = CloudEvent::new(msg);
            let nc: &NatsClient<T> = &self.nats.borrow();
            let response = serde_json::to_string(&response).unwrap();
            Ok(nc.publish_message(subj, response, None).await?)
        }

        pub async fn request_ce(&self, subj: String, msg: T) -> Result<Message, Error> {
            let response: CloudEvent<T> = CloudEvent::new(msg);
            let nc: &NatsClient<T> = &self.nats.borrow();
            let response = serde_json::to_string(&response).unwrap();
            let msg = nc.request_message(subj, response, 1).await?;
            Ok(msg)
        }

        pub async fn add_subscription(
            &mut self,
            subj: String,
            handle: fn(&'_ Subscription) -> BoxFuture<'_, T>,
        ) -> Result<(), Error> {
            let mut nc = self.nats.borrow_mut();
            nc.add_subscription(subj, handle).await?;
            Ok(())
        }

        pub async fn handle_subscription(&self, subj: String) -> Result<T, Error> {
            let nc = self.nats.borrow();
            Ok(nc.handle_subscription(subj).await?)
        }
    }

    pub fn set_dto<'a, T>(msg: &'a str) -> Result<T, Error>
    where
        T: Sized + Deserialize<'a>,
    {
        let dto = serde_json::from_str(msg)?;
        Ok(dto)
    }
}
